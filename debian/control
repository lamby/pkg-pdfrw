Source: pdfrw
Section: python
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper (>= 7.0.50~)
Build-Depends-Indep: python-support, python-setuptools
Standards-Version: 3.9.2
Homepage: http://code.google.com/p/pdfrw/
Vcs-Git: git://github.com/lamby/pkg-pdfrw.git
Vcs-Browser: https://github.com/lamby/pkg-pdfrw

Package: python-pdfrw
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, python-reportlab
Replaces: pdfrw
Provides: pdfrw
Conflicts: pdfrw
Description: PDF file manipulation library
 pdfrw can read and write PDF files, and can also be used to read in PDFs which
 can then be used inside reportlab.
 .
 pdfrw tries to be agnostic about the contents of PDF files, and support them
 as containers, but to do useful work, something a little higher-level is
 required. It supports the following:
 .
  * PDF pages. pdfrw knows enough to find the pages in PDF files you read in,
    and to write a set of pages back out to a new PDF file.
  * Form XObjects. pdfrw can take any page or rectangle on a page, and convert
    it to a Form XObject, suitable for use inside another PDF file
  * reportlab objects. pdfrw can recursively create a set of reportlab objects
    from its internal object format. This allows, for example, Form XObjects to
    be used inside reportlab.
